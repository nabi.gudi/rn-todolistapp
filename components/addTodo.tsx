import React, { useState } from 'react';
import {StyleSheet, Text, View, TextInput, Button, TouchableOpacity} from 'react-native';

export interface Props {
  onSubmit: (text: string) => void;
}

export default function AddTodo({onSubmit}: Props){
  const [text, setText] = useState('');

  const changeHandler = (value: string) => {
      setText(value);
  }
  return (
    <View>
      <TextInput 
        style={styles.input}
        placeholder= 'new todo...'
        onChangeText={changeHandler}
      />

      <TouchableOpacity style={styles.button} onPress={() => onSubmit(text)}>
        <Text style={styles.buttonText}>Add Todo</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  input: {
    marginBottom: 10,
    paddingHorizontal: 8,
    paddingVertical: 6,
    borderBottomWidth: 1,
    borderBottomColor: '#ddd'
  },
  button: {
    backgroundColor: 'coral',
    padding: 10,
    justifyContent: 'center',
    borderRadius: 10
  }, 
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold'
  }
});
