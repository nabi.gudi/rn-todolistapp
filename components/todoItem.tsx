import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';

export interface TodoItem {
  text: string;
  key: string;
}

export interface Props {
  todo: TodoItem;
  onPress: (key: string) => void;
}

export default function Header({todo, onPress}: Props){
  return (
    <TouchableOpacity onPress={() => onPress(todo.key)}>
      <Text style={styles.text}>{todo.text}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  text: {
    padding: 16,
    marginTop: 16,
    borderColor: '#bbb',
    borderWidth: 1,
    borderStyle: 'dashed',
    borderRadius: 10
  },
});
